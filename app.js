// Flag name arrays
const easy = [
  'argentina',
  'australia',
  'austria',
  'bahamas',
  'belarus',
  'belgium',
  'bolivia',
  'brazil',
  'bulgaria',
  'canada',
  'chile',
  'china',
  'colombia',
  'croatia',
  'cuba',
  'czech-republic',
  'denmark',
  'egypt',
  'finland',
  'france',
  'germany',
  'greece',
  'iceland',
  'india',
  'ireland',
  'israel',
  'italy',
  'japan',
  'netherlands',
  'new zealand',
  'norway',
  'poland',
  'portugal',
  'russia',
  'spain',
  'sweden',
  'switzerland',
  'united kingdom',
  'united states of america'
];
const medium = [
  'afghanistan',
  'albania',
  'algeria',
  'andorra',
  'armenia',
  'azerbaijan',
  'bangladesh',
  'bosnia and herzegovina',
  'cambodia',
  'cameroon',
  'costa rica',
  'cyprus',
  'ecuador',
  'estonia',
  'ethiopia',
  'fiji',
  'georgia',
  'haiti',
  'hungary',
  'indonesia',
  'iran',
  'iraq',
  'jamaica',
  'kazakhstan',
  'kenya',
  'kuwait',
  'kyrgyzstan',
  'latvia',
  'lebanon',
  'libya',
  'liechtenstein',
  'lithuania',
  'monaco',
  'niger',
  'nigeria',
  'north korea',
  'oman',
  'pakistan',
  'panama',
  'papua new guinea',
  'paraguay',
  'peru',
  'philippines',
  'qatar',
  'romania',
  'rwanda',
  'san marino',
  'saudi arabia',
  'serbia',
  'singapore',
  'slovakia',
  'slovenia',
  'south africa',
  'south korea',
  'sri lanka',
  'syria',
  'taiwan',
  'tajikistan',
  'tanzania',
  'thailand',
  'trinitada and tobago',
  'tunisia',
  'turkey',
  'turkmenistan',
  'uganda',
  'ukrain',
  'united arab emirates',
  'uruguay',
  'uzbekistan',
  'vatican city',
  'venezuela',
  'vietnam',
  'zambia',
  'zimbabwe'
];
const hard = [
  'angola',
  'antigua and barbuda',
  'bahrain',
  'barbados',
  'belize',
  'benin',
  'bhutan',
  'botswana',
  'brunei',
  'burkina faso',
  'burundi',
  'cape verde',
  'central african republic',
  'chad',
  'comoros',
  'congo democratic republic',
  'congo republic',
  'cote d ivoire',
  'djibouti',
  'dominica',
  'dominican republic',
  'east timor',
  'el salvador',
  'equatorial guinea',
  'eritrea',
  'gabon',
  'gambia',
  'ghana',
  'grenada',
  'guatemala',
  'guinea bissau',
  'guinea',
  'guyana',
  'honduras',
  'jordan',
  'kiribati',
  'kosovo',
  'laos',
  'lesotho',
  'liberia',
  'nicaragua',
  'niue',
  'palau',
  'saint kitts and nevis',
  'samoa',
  'sao tome and principe',
  'senegal',
  'seychelles',
  'sierra leone',
  'solomon islands',
  'somalia',
  'south sudan',
  'sudan',
  'suriname',
  'swaziland',
  'togo',
  'tonga',
  'tuvalu',
  'yemen'
];

// UI vars
const game = document.querySelector('#game');
const level = document.querySelectorAll('.level-game');
const flag = document.querySelectorAll('.flag');
const flagOne = document.querySelector('.flag-one');
const flagTwo = document.querySelector('.flag-two');
const flagThree = document.querySelector('.flag-three');
const flagFour = document.querySelector('.flag-four');
const modal = document.querySelector('#modal1');
const modalClose = document.querySelector('.modal-close');
let points = document.querySelectorAll('.points-heading');
let flagName = document.querySelector('.flag-name');
let countDown = document.querySelector('.time-left');

const instance = M.Modal.init(modal);
let randomNums = new Array(4);
let gameLevel = '';
let gamePoints = 0;
let streak = 1;
let timeLeft = 30;

let easyFlags = [];
for (let ef in easy) {
  easyFlags[ef] = new Image();
  easyFlags[ef] = `./img/easy/${easy[ef]}-flag-medium.png`;
}

let mediumFlags = [];
for (let mf in medium) {
  mediumFlags[mf] = new Image();
  mediumFlags[mf] = `./img/medium/${medium[mf]}-flag-medium.png`;
}

let hardFlags = [];
for (let hf in hard) {
  hardFlags[hf] = new Image();
  hardFlags[hf] = `./img/hard/${hard[hf]}-flag-medium.png`;
}
// console.log('EASY FLAGS', easyFlags)
// console.log('MEDIUM FLAGS', mediumFlags)
// console.log('HARD FLAGS', hardFlags)

// Loads initial window
document.addEventListener('DOMContentLoaded', chooseLevel);

function chooseLevel() {
  game.style.display = 'none';
  document.querySelector('#level').style.display = 'block';
  points[0].textContent = gamePoints;
  for (let i = 0; i < level.length; i++) {
    level[i].addEventListener('click', function() {
      gameLevel = level[i].value;
      if (gameLevel) {
        game.style.display = 'block';
        setInterval(countdown, 1000);
        timeLeft = 30;
        document.querySelector('#level').style.display = 'none';
        getFlags();
      }
    });
  }
}

function countdown() {
  if (timeLeft == 0) {
    points[1].textContent = gamePoints;
    instance.open();
  } else {
    countDown.textContent = timeLeft + ' seconds remaining';
    timeLeft--;
  }
}

// Event listeners
for (let i = 0; i < flag.length; i++) {
  flag[i].addEventListener('mousedown', function() {
    const flagSrc = flag[i].src;
    let flagNameFormatted = flagSrc.slice(
      flagSrc.lastIndexOf('/') + 1,
      flagSrc.indexOf('flag-medium') - 1
    );
    flagNameFormatted = flagNameFormatted.replace(/-/g, ' ');

    if (flagNameFormatted === flagName.textContent) {
      switch (gameLevel) {
        case 'easy':
          gamePoints += 100 * streak;
          break;
        case 'medium':
          gamePoints += 200 * streak;
          break;
        case 'hard':
          gamePoints += 300 * streak;
          break;
      }
      streak += 1;
      getFlags();
    } else {
      streak = 1;
    }
  });
}

function getFlags() {
  let i = 0;
  points[0].textContent = gamePoints;
  while (i < randomNums.length) {
    let randomNum = getRandomFlag(0, easy.length - 1);

    let index = randomNums.indexOf(randomNum);
    if (index === -1) {
      randomNums[i] = randomNum;
      i++;
    }
  }

  // Load flags into img
  const flagArr = new Array(4);
  switch (gameLevel) {
    case 'easy':
      for (let f = 0; f < flag.length; f++) {
        flagArr[f] = easy[randomNums[f]].replace(/ /g, '-');
      }
      break;
    case 'medium':
      for (let f = 0; f < flag.length; f++) {
        flagArr[f] = medium[randomNums[f]].replace(/ /g, '-');
      }
      break;
    case 'hard':
      for (let f = 0; f < flag.length; f++) {
        flagArr[f] = hard[randomNums[f]].replace(/ /g, '-');
      }
      break;
  }

  // Get Flag to guess
  let randomNum = getRandomFlag(0, randomNums.length - 1);
  flagName.textContent = flagArr[randomNum].replace(/-/g, ' ');

  flagOne.src = `./img/${gameLevel}/${flagArr[0]}-flag-medium.png`;
  flagTwo.src = `./img/${gameLevel}/${flagArr[1]}-flag-medium.png`;
  flagThree.src = `./img/${gameLevel}/${flagArr[2]}-flag-medium.png`;
  flagFour.src = `./img/${gameLevel}/${flagArr[3]}-flag-medium.png`;
}

// Modal window
modalClose.addEventListener('click', function() {
  instance.close();
  gamePoints = 0;
  streak = 1;
  timeLeft = 30;
  chooseLevel();
});

// Gets random number
function getRandomFlag(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
